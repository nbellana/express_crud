//Importing modules
const express = require("express");
const mysql = require("mysql");

const app = express();
const PORT = 8080;

// Create a connection to the database
const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root123",
  database: "neelima_db",
});

// open the MySQL connection
const runAQuery = (query) => {
  con.connect((err) => {
    if (err) {
      console.log("Error connecting to Db");
      return;
    }
    console.log("Connection established");
  });

  con.query(query, function (err, result) {
    console.log(result);
    if (err) throw err;
    console.log("query executed!");
  });
};

/**
 *
 * @param {*} dbName database name to be created
 * @returns sql query to create a database
 */
const createDB = (dbName) => {
  return `CREATE DATABASE ${dbName}`;
};

//Function to create table
const createTable = (tablename, col1, col2, col3, col4) => {
  return `CREATE TABLE ${tablename} (${col1} VARCHAR(50), ${col2} int(10), ${col3} int(10), ${col4} int(10))`;
};

//Function to specify the database to be used
const useDB = (dbName) => {
  return `use ${dbName}`;
};

//Function to read metadata of the table
const readTable = (tableName) => {
  return `DESC ${tableName}`;
};

//Function to add primary key by altering table
const addPrimaryKey = (tableName, constraint, colName) => {
  return `ALTER TABLE ${tableName} ADD CONSTRAINT ${constraint} PRIMARY KEY (${colName})`;
};

//Function to insert records
const insertRecords = (
  tableName,
  col1,
  col2,
  col3,
  col4,
  value1,
  value2,
  value3,
  value4
) => {
  return `INSERT INTO ${tableName} (${col1}, ${col2}, ${col3}, ${col4}) VALUES (${value1}, ${value2}, ${value3}, ${value4})`;
};

//Function to read records
const readRecords = (tableName) => {
  return `SELECT * FROM ${tableName}`;
};

//Function to update records
const updateRecords = (tableName, updateCol, updateValue, keyCol, keyValue) => {
  return `UPDATE ${tableName} SET ${updateCol} = '${updateValue}' WHERE ${keyCol} = ${keyValue}`;
};

//Function to delete records
const deleteRecords = (tableName, keyCol, keyValue) => {
  return `DELETE FROM ${tableName} WHERE ${keyCol}='${keyValue}'`;
};

//Function to delete table
const deleteTable = (tableName) => {
  return `DROP TABLE ${tableName}`;
};

//Function to delete database
const deleteDB = (dbName) => {
  return `DROP DATABASE ${dbName}`;
};

//Create database
app.post("/1", function (req, res) {
  res.send(runAQuery(createDB("neelimaaa_db")));
});

//Read database
app.put("/2", function (req, res) {
  res.send(runAQuery(useDB("neelima_db")));
});

//Create table
app.post("/3", function (req, res) {
  res.send(
    runAQuery(createTable("bookstore", "item_name", "id", "price", "quantity"))
  );
});

//Read table
app.get("/4", function (req, res) {
  res.send(runAQuery(readTable("bookstore")));
});

//Update table
app.put("/5", function (req, res) {
  res.send(runAQuery(addPrimaryKey("bookstore", "BS_KEY", "id")));
});

//Create records
app.post("/6", function (req, res) {
  res.send(
    runAQuery(
      insertRecords(
        "bookstore",
        "item_name",
        "id",
        "price",
        "quantity",
        "'Pen'",
        "'1'",
        "'50'",
        "'100'"
      )
    )
  );
});

//Read records
app.get("/7", function (req, res) {
  res.send(runAQuery(readRecords("bookstore")));
});

//Update records
app.put("/8", function (req, res) {
  res.send(runAQuery(updateRecords("bookstore", "price", "20", "id", "1")));
});

//Delete records
app.delete("/9", function (req, res) {
  res.send(runAQuery(deleteRecords("bookstore", "id", "1")));
});

//Delete table
app.delete("/10", function (req, res) {
  res.send(runAQuery(deleteTable("bookstore")));
});

//Delete database
app.delete("/11", function (req, res) {
  res.send(runAQuery(deleteDB("neelimadb")));
});

//Listening to port
app.listen(PORT, () => {
  console.log(`listening on port ${PORT}!`);
});
